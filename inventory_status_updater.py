# inventory_status_updater.py
# written by: Nate
# last udpated: 9/7/2017
# This script will perform an update to customer's inventory using the API to update asset IDs with status

import csv
import requests

# infile should look like
# Asset ID,Status
# 12345,Confirmed
# 12346,
infile = 'inventoryupdate.csv'
# https://sf.riskiq.net/crawlview/api/docs/schemas/V1InventoryUpdateAsset.html#property_status
default_status = 'Confirmed'
auth = ('token', 'privateKey')
url = 'https://ws.riskiq.net/v1/inventory/update'

by_status = {}
with open(infile) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        asset_id = row.get('Asset ID')
        status = row.get('Status') or default_status
        if not status:
            raise ValueError('No Status or Default Status provided.')
        by_status[status] = by_status.get(status, []) + [asset_id]

for status, asset_ids in by_status.items():
    data = {
        'ids': asset_ids,
        'status': status
    }
    #requests.post(url, json=data, auth=auth)
    response = requests.post(url, json=data, auth=auth)
    print(response.text)